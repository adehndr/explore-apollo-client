package com.example.rocketreserver.UI

import android.database.Cursor
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ListView
import android.widget.SimpleCursorAdapter
import android.widget.TextView
import com.example.rocketreserver.SQLite.DBManager
import com.example.rocketreserver.SQLite.DatabaseHelper
import android.content.Intent
import android.util.Log
import android.view.Menu
import android.view.MenuItem

import android.widget.AdapterView.OnItemClickListener
import com.example.rocketreserver.ModifyCountryActivity
import com.example.rocketreserver.R


class EntryActivity : AppCompatActivity() {

    val from = arrayOf(
        DatabaseHelper._ID,
        DatabaseHelper.NUMBER
    )
    val to = intArrayOf(R.id.id_sqlite, R.id.NUMBER)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_entry)

        val dbManager : DBManager = DBManager(this)
        dbManager.open()
        val cursor : Cursor? = dbManager.fetch()

        val listView = findViewById<ListView>(R.id.list_view)
        listView.emptyView = findViewById(R.id.empty)
        val adapter : SimpleCursorAdapter = SimpleCursorAdapter(this,
            R.layout.view_record, cursor,from,to,0)
        listView.adapter = adapter
        listView.onItemClickListener =
            OnItemClickListener { parent, view, position, viewId ->
                val idTextView = view.findViewById(R.id.id_sqlite) as TextView
                val titleTextView = view.findViewById(R.id.NUMBER) as TextView
                val id = idTextView.text.toString()
                val title = titleTextView.text.toString()
                val modify_intent = Intent(applicationContext, ModifyCountryActivity::class.java)
                modify_intent.putExtra("title", title)
                modify_intent.putExtra("id", id)
                startActivity(modify_intent)
            }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_country,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId)
        {
            R.id.add_item -> {
                startActivity(Intent(this,AddCountryActivity::class.java))
                true
            }
            else -> return super.onOptionsItemSelected(item)
        }

    }
}