package com.example.rocketreserver.SQLite

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DatabaseHelper(context: Context?) :
    SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION) {
    companion object {
        // Table Name
        const val TABLE_NAME = "COUNTRIES"

        // Table columns
        const val _ID = "_id"
        const val NUMBER = "number"

        // Database Information
        const val DB_NAME = "JOURNALDEV_COUNTRIES.DB"

        // database version
        const val DB_VERSION = 1

        // Creating table query
        private const val CREATE_TABLE = "create table " + TABLE_NAME + "(" + _ID +" INTEGER PRIMARY KEY AUTOINCREMENT, "+ NUMBER + " TEXT NOT NULL);"
    }
    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(CREATE_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME)
        onCreate(db)
    }

}