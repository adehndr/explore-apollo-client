package com.example.rocketreserver.SQLite

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.SQLException

import android.database.sqlite.SQLiteDatabase


class DBManager(private val context: Context) {
    private var dbHelper: DatabaseHelper? = null
    private var database: SQLiteDatabase? = null
    @Throws(SQLException::class)
    fun open(): DBManager {
        dbHelper = DatabaseHelper(context)
        database = dbHelper!!.writableDatabase
        return this
    }

    fun close() {
        dbHelper!!.close()
    }

    fun insert(num: String?) {
        val contentValue = ContentValues()
        contentValue.put(DatabaseHelper.NUMBER, num)
        database!!.insert(DatabaseHelper.TABLE_NAME, null, contentValue)
    }

    fun fetch(): Cursor? {
        val columns = arrayOf(DatabaseHelper._ID, DatabaseHelper.NUMBER)
        val cursor: Cursor? =
            database!!.query(DatabaseHelper.TABLE_NAME, columns, null, null, null, null, null)
        if (cursor != null) {
            cursor.moveToFirst()
        }
        return cursor
    }

    fun queryAllID() : ArrayList<String>
    {
        val mArrayList = ArrayList<String>()
        val queryFirstData = "SELECT ${DatabaseHelper.NUMBER} FROM ${DatabaseHelper.TABLE_NAME}"
        val cursor = database?.rawQuery(queryFirstData,null)
        if (cursor != null) {
            cursor.moveToFirst()
            while (!cursor.isAfterLast)
            {
                mArrayList.add(cursor.getString(0))
                cursor.moveToNext()
            }
            cursor.close()
        }
        return mArrayList
    }

    fun getFirstData(): String {
        var idQuery : String = "104"
        val queryFirstData = "SELECT ${DatabaseHelper.NUMBER} FROM ${DatabaseHelper.TABLE_NAME}"
        val cursor = database!!.rawQuery(queryFirstData,null)
        if (cursor.moveToFirst())
        {
            idQuery = cursor.getString(0)
        }
        return idQuery
    }

    fun update(_id: Long, num: String?): Int {
        val contentValues = ContentValues()
        contentValues.put(DatabaseHelper.NUMBER, num)
        return database!!.update(
            DatabaseHelper.TABLE_NAME,
            contentValues,
            DatabaseHelper._ID + " = " + _id,
            null
        )
    }

    fun delete(_id: Long) {
        database!!.delete(DatabaseHelper.TABLE_NAME, DatabaseHelper._ID + "=" + _id, null)
    }
}