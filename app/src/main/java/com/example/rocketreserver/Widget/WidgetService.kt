package com.example.rocketreserver.Widget

import android.appwidget.AppWidgetManager
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.RemoteViews
import android.widget.RemoteViewsService
import com.apollographql.apollo.coroutines.await
import com.apollographql.apollo.exception.ApolloException
import com.example.rocketreserver.API.Apollo
import com.example.rocketreserver.LaunchDetailQuery
import com.example.rocketreserver.R
import com.example.rocketreserver.SQLite.DBManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class WidgetService : RemoteViewsService() {
    override fun onGetViewFactory(intent: Intent?): RemoteViewsFactory {
        return WidgetItemFactory(applicationContext,intent!!)
    }

    internal inner class WidgetItemFactory(private val context : Context, private val intent : Intent) : RemoteViewsFactory{
/*        private val listId  = arrayOf(
            "one", "two", "three", "four",
            "five", "six", "seven", "eight", "nine", "ten"
        )*/
        private val listId = arrayListOf<String>()
        private val listNameLaunch = arrayListOf<String>()
        private val appWidgetId: Int
        private lateinit var dbManager: DBManager
        init {
            appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,AppWidgetManager.INVALID_APPWIDGET_ID)
        }

        override fun onCreate() {
            val apolloClient = Apollo().getApolloClient()
            dbManager = DBManager(this@WidgetService)
            dbManager.open()
            listId.addAll(dbManager.queryAllID())
            Log.d("IsiList", listId.toString())
            GlobalScope.launch {
                listId.forEach {
                    val response = try {
                        apolloClient?.query(LaunchDetailQuery(id = it))?.await()
                    }catch (e : ApolloException)
                    {
                        Log.d("Widget",e.message)
                        return@launch
                    }
                    val launch = response?.data?.launch
                    if (launch == null || response.hasErrors()){
                        return@launch
                    }
                    launch.site?.let { it1 -> listNameLaunch.add(it1) }
                }
                Log.d("IsiListGraphQL",listNameLaunch.toString())
            }

        }

        override fun onDataSetChanged() {

        }


        override fun onDestroy() {
//            dbManager.close()
//            listId.clear()
        }

        override fun getCount(): Int {
            return listNameLaunch.size
        }

        override fun getViewAt(position: Int): RemoteViews {
            val views = RemoteViews(context.packageName, R.layout.widget_layout_item)
            views.setTextViewText(R.id.widget_item_text,listNameLaunch[position])
            return views
        }

        override fun getLoadingView(): RemoteViews? {
            return null
        }

        override fun getViewTypeCount(): Int {
            return 1
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun hasStableIds(): Boolean {
            return true
        }
    }
}