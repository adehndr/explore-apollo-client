package com.example.rocketreserver.Widget
import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.widget.RemoteViews
import com.apollographql.apollo.coroutines.await
import com.apollographql.apollo.exception.ApolloException
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.AppWidgetTarget
import com.example.rocketreserver.API.Apollo
import com.example.rocketreserver.LaunchDetailQuery
import com.example.rocketreserver.R
import com.example.rocketreserver.SQLite.DBManager
import com.example.rocketreserver.UI.MainActivity
import kotlinx.coroutines.*

class ExampleAppWidgetProvider : AppWidgetProvider() {
    companion object{
        const val ACTION_TOAST = "actionToast"
        const val EXTRA_ITEM_POSITION ="extraItemPosition"
    }

    override fun onUpdate(
        context: Context?,
        appWidgetManager: AppWidgetManager?,
        appWidgetIds: IntArray
    ) {
/*        var idGraphQl : String = "104"
        val dbManager : DBManager? = context?.let { DBManager(it) }
        if (dbManager != null) {
            dbManager.open()
        }
        if (dbManager != null) {
            idGraphQl = dbManager.getFirstData()
        }
        dbManager?.close()*/
        for (appWidgetId in appWidgetIds)
        {
            val intent = Intent(context, MainActivity::class.java)
            val pendingIntent = PendingIntent.getActivity(context, 0, intent, 0)

            val serviceIntent = Intent(context,WidgetService::class.java)
            serviceIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,appWidgetId)
            serviceIntent.setData(Uri.parse(serviceIntent.toUri(Intent.URI_INTENT_SCHEME)))

            val remoteViews : RemoteViews = RemoteViews(context?.packageName,
                R.layout.widget_layout
            )
            remoteViews.setOnClickPendingIntent(R.id.button_widget,pendingIntent)
            remoteViews.setRemoteAdapter(R.id.stack_view,serviceIntent)
            remoteViews.setEmptyView(R.id.stack_view,R.id.empty_text_view)
            appWidgetManager?.updateAppWidget(appWidgetId,remoteViews)
/*            GlobalScope.launch(Dispatchers.IO) {
                val response = try {
                    Apollo().getApolloClient()?.query(LaunchDetailQuery(id = idGraphQl))?.await()
                }catch (e : ApolloException)
                {
                    Log.d("Widget",e.message)
                    return@launch
                }
                val launch = response?.data?.launch
                if (launch == null || response.hasErrors()){
                    return@launch
                }
                val remoteViews : RemoteViews = RemoteViews(context?.packageName,
                    R.layout.widget_layout
                )
                withContext(Dispatchers.Main){
                    Log.d("WidgetLaunch", launch.mission?.name)
                    val appWidgetTarget = AppWidgetTarget(context,
                        R.id.widget_image,remoteViews,appWidgetId)
                    if (context != null) {
                        Glide.with(context.applicationContext)
                            .asBitmap()
                            .load(launch.mission?.missionPatch)
                            .into(appWidgetTarget)
                    }
                    remoteViews.setTextViewText(R.id.widget_text, launch.mission?.name)
                    remoteViews.setOnClickPendingIntent(R.id.button_widget,pendingIntent)
                    appWidgetManager?.updateAppWidget(appWidgetId,remoteViews)
                }
            }*/
        }
        super.onUpdate(context, appWidgetManager, appWidgetIds)
    }
}